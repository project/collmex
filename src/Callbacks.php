<?php

namespace Drupal\collmex;

class Callbacks {

  public function not($v) {
    return !$v;
  }

  public function minus($v) {
    return -$v;
  }

}
